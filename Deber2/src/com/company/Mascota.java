package com.company;

import java.security.PublicKey;

/**
 * Created by Lein on 14/05/2017.
 */
public class Mascota implements Cantante {
    public String nombreMascota;
    public String especie;
    public boolean cantante;
    public String movimiento;

    public void cantar(boolean canta){
        if(canta){
            System.out.println("Si CANTA");
        }
        else {
            System.out.println("No CANTA");
        }
    }

    public String especie(){
        return "Animal";
    }
}
