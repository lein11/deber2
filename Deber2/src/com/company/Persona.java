package com.company;

/**
 * Created by Lein on 14/05/2017.
 */
public class Persona  extends Mascota {
    String nombre;
    int edad;
    final int maxEdad = 100;

    public String id(String CI){
        return "CI: "+ CI;
    }

    public String id(String CI, String pasaporte){
        return "CI: "+ CI + "Pasaporte" + pasaporte;
    }

    public String especie(){
        return "Humano";
    }

}
