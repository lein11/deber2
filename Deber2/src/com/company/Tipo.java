package com.company;

/**
 * Created by Lein on 15/05/2017.
 */
public enum Tipo{
    AVE, PEZ, MAMIFERO, REPTIL;

    public String movimiento(){
        switch (this){
            case AVE:
                return "Vuela";
            case PEZ:
                return "Nada";
            case REPTIL:
                return "repta";
            case MAMIFERO:
                return "camina";
            default:
                return "Vacio";
        }
    }



}
